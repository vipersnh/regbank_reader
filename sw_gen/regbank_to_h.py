import sys; sys.path.append("../client")
import re
from sys import stdout
from pdb import set_trace
from regbank_to_db import regbank_database_t
from regbank_parser import regbank_t

if __name__ =="__main__":
    import os
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('fnames', nargs=1)
    parse_res = parser.parse_args()
    regbank_xl_fname = parse_res.fnames[0]

    db_gen = regbank_database_t(regbank_xl_fname, "module")
    database = db_gen.generate_regbank_database()
    regbank = regbank_t(database)


    generated_modules = {}
    for module in regbank:
        if module._module_name not in generated_modules.keys():
            generated_modules[module._module_name] = module

            sw_code_file = open(module._module_name + ".h", "w")
            text = "#ifndef __{0}_H__ \n#define __{0}_H__\n\n".format(module._module_name)
            sw_code_file.write(text)
            text = "\n/* Generated software access code for registers in module {0} */".format(module._module_name)
            sw_code_file.write(text)
            for register in module:
                word_offset = int(register._get_offset()/4)

                text = "\n\n\n\n\n/* Code for register {0}: At word offset {1} */".format(register._register_name,
                        word_offset)
                sw_code_file.write(text)
                text = "\n\n/* READ API for register {0} */".format(register._register_name)
                sw_code_file.write(text)
                for field in register:
                    if re.search("RESERVE", field._subfield_name.upper()):
                        continue

                    bit_pos = field._bit_position[0]
                    bit_len = field._bit_width

                    text = "\n\n#define {0}_{1}_{2}_READ(_base)\t\t".format(module._module_name.upper(),
                            register._register_name.upper(), field._subfield_name.upper())
                    text += "((((unsigned int *)(_base)[{0}]) >> {1}) & ((1<<{2})-1))".format(word_offset,
                            bit_pos, bit_len)
                    sw_code_file.write(text)

                text = "\n\n\n/* WRITE API for register {0} */".format(register._register_name)
                sw_code_file.write(text)
                for field in register:
                    if re.search("RESERVE", field._subfield_name.upper()):
                        continue

                    bit_pos = field._bit_position[0]
                    bit_len = field._bit_width

                    text = "\n\n#define {0}_{1}_{2}_WRITE(_base, _val)\t\t".format(module._module_name.upper(),
                            register._register_name.upper(), field._subfield_name.upper())

                    text += "((unsigned int *)(_base)[{0}] | (((_val) & ((1<<{1})-1)) << {2}))".format(word_offset,
                            bit_len, bit_pos)
                    sw_code_file.write(text)
            text = "\n\n#endif"
            sw_code_file.write(text)
            sw_code_file.close()
